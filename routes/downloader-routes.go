package routes

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/0x113/movie-downloader/models"
	"github.com/0x113/movie-downloader/scraper"
	"github.com/gorilla/mux"
)

func MovieInfoEndpoint(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	params := mux.Vars(r)
	movieDetails, err := scraper.GetMovieDetails("http://cda.pl/video/" + params["id"])
	if err != nil {
		json.NewEncoder(w).Encode(models.Exception{Error: "Wrong movie link"})
		return
	}

	json.NewEncoder(w).Encode(movieDetails)

}

func MovieDownloadLinkEndpoint(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	params := mux.Vars(r)
	embed := scraper.CreateEmbedLink(params["id"])
	downloadLink := scraper.GetDownloadLink(embed, params["quality"], params["id"])

	json.NewEncoder(w).Encode(models.Download{DownloadLink: downloadLink})
}

func FiliMovieInfoEndpoint(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	params := mux.Vars(r)
	movieURL := fmt.Sprintf("https://fili.cc/film/%s/%s", params["title"], params["id"])
	movieInfo, err := scraper.GetFiliMovieInfo(movieURL)
	if err != nil {
		json.NewEncoder(w).Encode(models.Exception{Error: "Error while getting info about movie"})
		return
	}

	json.NewEncoder(w).Encode(movieInfo)

}

func FiliAudioTypeInfoEndpoint(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	params := mux.Vars(r)

	movieURL := fmt.Sprintf("https://fili.cc/film/%s/%s", params["title"], params["id"])
	hostInfoList, err := scraper.GetTypeInfo(movieURL, params["audio"])
	if err != nil {
		json.NewEncoder(w).Encode(models.Exception{Error: "Error with getting info about hosts"})
		return
	}

	json.NewEncoder(w).Encode(hostInfoList)
}

func FiliMovieDownloadEndpoint(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	params := mux.Vars(r)
	embed, err := scraper.GetSourceEmbed(params["salt"])
	if err != nil {
		json.NewEncoder(w).Encode(models.Exception{Error: "Error while getting embed url"})
		return
	}

	host := params["host"]
	if host == "openload" {
		downloadLink := scraper.OpenloadDownload(embed)
		json.NewEncoder(w).Encode(models.Download{DownloadLink: downloadLink})
		return
	}
	if host == "vidoza" {
		downloadLink, err := scraper.VidozaDownload(embed)
		if err != nil {
			json.NewEncoder(w).Encode(models.Exception{Error: "Error while downloading movie"})
			return
		}

		json.NewEncoder(w).Encode(models.Download{DownloadLink: downloadLink})
		return
	}
	json.NewEncoder(w).Encode(models.Exception{Error: "At this moment we can't download movie from that host"})
}

// TV SERIES

func FiliSeriesEpisodesEndpoint(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	params := mux.Vars(r)

	url := fmt.Sprintf("https://fili.cc/serial/%s/%s", params["title"], params["id"])
	episodes, err := scraper.GetSeasons(url)
	if err != nil {
		json.NewEncoder(w).Encode(models.Exception{Error: "Error while getting seassons"})
		return
	}

	json.NewEncoder(w).Encode(episodes)
}

func FiliEpisodeHostsEndpoint(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	params := mux.Vars(r)
	url := fmt.Sprintf("https://fili.cc/serial/%s/%s/%s/%s", params["series"], params["episode"], params["episode_title"], params["id"])
	episodeHosts, err := scraper.GetEpisodeHosts(url)
	if err != nil {
		json.NewEncoder(w).Encode(models.Exception{Error: "Error while getting hosts"})
		return
	}

	json.NewEncoder(w).Encode(episodeHosts)
}
