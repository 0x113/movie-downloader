package scraper

import (
	"log"
	"strings"

	"github.com/0x113/movie-downloader/models"
	"github.com/anaskhan96/soup"
)

func GetVideoID(videoURL string) string {
	s := strings.Split(videoURL, "/video/")
	return s[1]
}

func CreateEmbedLink(videoID string) string {
	return "http://ebd.cda.pl/488x310/" + videoID
}

func GetMovieDetails(videoURL string) (models.CDAInfo, error) {
	var qualityList []string
	res, err := soup.Get(videoURL)
	if err != nil {
		return models.CDAInfo{}, err
	}
	doc := soup.HTMLParse(res)

	// get quality list
	qualityButtons := doc.FindAll("a", "class", "quality-btn")
	for _, btn := range qualityButtons {
		qualityList = append(qualityList, btn.Text())
	}

	// get title
	title := doc.Find("meta", "property", "og:title").Attrs()["content"]

	return models.CDAInfo{Title: title, QualityList: qualityList}, nil

}

func GetDownloadLink(embedURL, quality, id string) string {
	var fullLink string
	// fullLink := embedURL + "?wersja=" + quality
	if quality == "nieznana" {
		fullLink = embedURL
	} else {
		fullLink = embedURL + "?wersja=" + quality
	}

	res, err := soup.Get(fullLink)
	if err != nil {
		log.Fatal(err)
	}

	doc := soup.HTMLParse(res)
	playerData := doc.Find("div", "id", "mediaplayer"+id).Attrs()["player_data"]
	videoObject := strings.Split(playerData, `file":"`)
	videoDownloadLink := strings.Split(videoObject[1], `","file_cast"`)[0]

	return videoDownloadLink
}
