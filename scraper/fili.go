package scraper

import (
	"fmt"
	"strings"

	"github.com/0x113/movie-downloader/models"
	"github.com/anaskhan96/soup"
)

func GetFiliMovieInfo(videoURL string) (models.FiliInfo, error) {
	var audioTypes []string
	res, err := soup.Get(videoURL)
	if err != nil {
		return models.FiliInfo{}, err
	}

	doc := soup.HTMLParse(res)
	// get info about movie
	title := doc.Find("h2", "class", "title").Text()
	description := doc.Find("p", "id", "desc").Text()
	genresString := doc.Find("span", "id", "genres").Text()
	genresString = strings.Replace(genresString, ",", "", -1)

	// convert string to array of strings
	genres := strings.Fields(genresString)

	releaseDate := doc.Find("span", "id", "date").Text()

	audioTypesHTML := doc.FindAll("div", "class", "l_btn")
	for _, v := range audioTypesHTML {
		audioTypes = append(audioTypes, v.Attrs()["data-type"])
	}

	movieInfo := models.FiliInfo{
		Title:       title,
		Description: description,
		Genres:      genres,
		ReleaseDate: releaseDate,
		AudioTypes:  audioTypes,
	}

	return movieInfo, nil
}

func GetTypeInfo(url, audioType string) ([]models.FiliLinkInfo, error) {
	var linkInfoList []models.FiliLinkInfo

	res, err := soup.Get(url)
	if err != nil {
		return linkInfoList, err
	}

	doc := soup.HTMLParse(res)
	liList := doc.Find("ul", "data-type", audioType).FindAll("li")
	for _, li := range liList {
		host := li.Find("span", "class", "host").Text()
		quality := li.Find("span", "class", "quality").Text()
		salt := li.Attrs()["data-ref"]

		linkInfo := models.FiliLinkInfo{
			Host:    host,
			Quality: quality,
			Salt:    salt,
		}
		linkInfoList = append(linkInfoList, linkInfo)
	}

	return linkInfoList, nil
}

func GetSourceEmbed(salt string) (string, error) {
	embed := fmt.Sprintf("https://fili.cc/embed?type=movie&code=%s&salt=%s", salt, salt)

	res, err := soup.Get(embed)
	if err != nil {
		return "", err
	}

	doc := soup.HTMLParse(res)
	script := doc.FindAll("script")[1].Text()
	script = strings.Split(script, "var url = '")[1]
	srcEmbed := strings.Split(script, "';")[0]

	return srcEmbed, nil
}

// OpenloadDownload returns link from where you can download movie
func OpenloadDownload(embed string) string {
	movieID := strings.Split(embed, "/embed/")[1]

	return "https://oload.site/f/" + movieID
}

// VidozaDownload returns link to video file
func VidozaDownload(embed string) (string, error) {
	res, err := soup.Get(embed)
	if err != nil {
		return "", err
	}

	doc := soup.HTMLParse(res)
	videoSrc := doc.Find("source", "type", "video/mp4").Attrs()["src"]
	return videoSrc, nil
}

// TV SERVIES DOWNLOAD
func GetSeasons(seriesURL string) ([]models.Season, error) {
	var seasons []models.Season
	res, err := soup.Get(seriesURL)
	if err != nil {
		return nil, err
	}

	doc := soup.HTMLParse(res)
	seasonEpisodes := doc.Find("div", "id", "episodes").FindAll("ul")
	for _, seasonEp := range seasonEpisodes {
		var season models.Season
		season.SeasonNumber = seasonEp.Attrs()["data-season-num"]
		for _, ep := range seasonEp.FindAll("li") {
			base := ep.Find("a", "class", "episodeName")
			episode := models.Episode{Title: base.Text(), EpisodeURL: base.Attrs()["href"]}
			season.Episodes = append(season.Episodes, episode)
		}

		seasons = append(seasons, season)
	}

	return seasons, nil
}

func GetEpisodeHosts(episodeURL string) ([]models.EpisodeInfo, error) {
	var hosts []models.EpisodeInfo
	res, err := soup.Get(episodeURL)
	if err != nil {
		return nil, err
	}

	doc := soup.HTMLParse(res)
	links := doc.Find("div", "id", "links").FindAll("ul")
	for _, link := range links[1:len(links)] {
		var episodeInfo models.EpisodeInfo
		episodeInfo.AudioType = link.Attrs()["data-type"]
		for _, hostInfo := range link.FindAll("li") {
			var host models.FiliLinkInfo
			host.Host = hostInfo.Find("span", "class", "host").Text()
			host.Quality = hostInfo.Find("span", "class", "quality").Text()
			host.Salt = hostInfo.Attrs()["data-ref"]
			episodeInfo.Hosts = append(episodeInfo.Hosts, host)
		}
		hosts = append(hosts, episodeInfo)
	}

	return hosts, nil
}
