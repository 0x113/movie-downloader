package main

import (
	"log"
	"net/http"
	"os"

	"github.com/0x113/movie-downloader/routes"
	"github.com/gorilla/mux"
)

func main() {
	r := mux.NewRouter()

	r.HandleFunc("/api/cda/movie/detail/{id}", routes.MovieInfoEndpoint).Methods("GET")
	r.HandleFunc("/api/cda/movie/download/{id}/{quality}", routes.MovieDownloadLinkEndpoint).Methods("GET")

	r.HandleFunc("/api/fili/movie/detail/{title}/{id}", routes.FiliMovieInfoEndpoint).Methods("GET")
	r.HandleFunc("/api/fili/movie/hosts/{title}/{id}/{audio}", routes.FiliAudioTypeInfoEndpoint).Methods("GET")
	r.HandleFunc("/api/fili/movie/download/{host}/{salt}", routes.FiliMovieDownloadEndpoint).Methods("GET")
	r.HandleFunc("/api/fili/series/episodes/{title}/{id}", routes.FiliSeriesEpisodesEndpoint).Methods("GET")
	r.HandleFunc("/api/fili/series/hosts/{series}/{episode}/{episode_title}/{id}", routes.FiliEpisodeHostsEndpoint)

	port := os.Getenv("PORT")
	if port == "" {
		port = "8000"
	}
	log.Printf("Serving http on port: %s", port)
	http.ListenAndServe(":"+port, r)
}
