package models

// CDAInfo model
type CDAInfo struct {
	Title       string   `json:"title"`
	QualityList []string `json:"quality_list"`
}

// CDADownload model
type Download struct {
	DownloadLink string `json:"download_link"`
}

// FiliInfo model
type FiliInfo struct {
	Title       string   `json:"title"`
	Description string   `json:"description"`
	Genres      []string `json:"genres"`
	ReleaseDate string   `json:"release_date"`
	AudioTypes  []string `json:"audio_types"`
}

type Season struct {
	SeasonNumber string    `json:"season_number"`
	Episodes     []Episode `json:"episodes"`
}

type Episode struct {
	Title      string `json:"title"`
	EpisodeURL string `json:"url"`
}

type EpisodeInfo struct {
	AudioType string         `json:"audio_type"`
	Hosts     []FiliLinkInfo `json:"hosts"`
}

// FiliLinkLinfo model
type FiliLinkInfo struct {
	Host    string `json:"host"`
	Quality string `json:"quality"`
	Salt    string `json:"salt"`
}

// Exception model
type Exception struct {
	Error string `json:"error"`
}
